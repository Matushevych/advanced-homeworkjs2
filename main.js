
/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

class HamburgerException extends Error {
    constructor(message) {
        super(message);
        this.name = "HamburgerError";
    }
}

class Hamburger {
    constructor(size, stuffing) {
        try {
            this._toppings = [];

            if (arguments.length) {
                this._size = size;
                this._stuffing = stuffing;
            } else {
                throw new HamburgerException('В функцію не передані параметри');
            }
        } catch (e) {
            if (e instanceof HamburgerException) {
                console.error(e.name + ': ' + e.message);
            } else {
                throw e;
            }
        }
    }
    addTopping(topping) {
        try {
            if (this._toppings.indexOf(topping) === -1) {
                this._toppings.push(topping);
            } else {
                throw new HamburgerException(`Добавка ${topping.name} вже у гамбургері`);
            }
        } catch (e) {
            if (e instanceof HamburgerException) {
                console.error(e.name + ': ' + e.message);
            } else {
                throw e;
            }
        }
    };

    removeTopping(topping) {
        try {
            let index = this._toppings.indexOf(topping);
            if (index !== -1) {
                this._toppings.splice(index, 1);
            } else {
                throw new HamburgerException(`Добавки ${topping.name} немає у гамбургері`);
            }
        } catch (e) {
            if (e instanceof HamburgerException) {
                console.error(e.name + ': ' + e.message);
            } else {
                throw e;
            }
        }
    };

    calculatePrice () {
        return this._toppings.reduce((prevVal, curVal) => prevVal + curVal.price, this._size.price + this._stuffing.price);
    };

    calculateCalories () {
        return this._toppings.reduce((prevVal, curVal) => prevVal + curVal.calories, this._size.calories + this._stuffing.calories);
    };

    get Toppings() {
        return this._toppings;
    }

    set Size(newSize) {
        try {
            if (!newSize) {
                throw new HamburgerException('Size is not given!');
            }
            if (newSize != Hamburger.SIZE_LARGE && newSize != Hamburger.SIZE_SMALL) {
                throw new HamburgerException('Invalid hamburger size ' + size);
            } else {
                return this._size = newSize;
            }
        } catch (e) {
            console.log(e);
        }
    }

    get Size() {
        return this._size;
    }

    set Stuffing(newStuffing) {
        if (!newStuffing) {
            throw new HamburgerException('Stuffing is not given!');
        }
        if (newStuffing != Hamburger.STUFFING_CHEESE && newStuffing != Hamburger.STUFFING_SALAD && newStuffing != Hamburger.STUFFING_POTATO) {
            throw new HamburgerException('Invalid hamburger stuffing ' + newStuffing);
        } else {
            this._stuffing = newStuffing;
        }
    }

    get Stuffing() {
        return this._stuffing;
    }


}

Hamburger.SIZE_SMALL = {
    name: 'small',
    calories: 20,
    price: 50
};

Hamburger.SIZE_LARGE = {
    name: 'large',
    calories: 40,
    price: 100
};

Hamburger.STUFFING_CHEESE = {
    name: 'cheese',
    calories: 10,
    price: 20
};

Hamburger.STUFFING_SALAD = {
    name: 'salad',
    calories: 5,
    price: 20
};

Hamburger.STUFFING_POTATO = {
    name: 'potato',
    calories: 10,
    price: 15
};

Hamburger.TOPPING_MAYO = {
    name: 'mayo',
    calories: 5,
    price: 20
};

Hamburger.TOPPING_SPICE = {
    name: 'spice',
    calories: 0,
    price: 15
};



// маленький гамбургер с начинкой из сыра

const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// // добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// // спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// // сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// // я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.Toppings.length); // 1
// // // А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// // Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.Size === Hamburger.SIZE_LARGE); // -> false
console.log("Is hamburger small: %s", hamburger.Size === Hamburger.SIZE_SMALL); // -> true
// // Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.Toppings.length); // 1
//
// // не передали обязательные параметры
// var h2 = new Hamburger(); // => HamburgerException: no size given
//
// // передаем некорректные значения, добавку вместо размера
// var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// // => HamburgerException: invalid size 'TOPPING_SAUCE'
//
// // добавляем много добавок
// var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
